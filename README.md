# HealthForge Coding Quiz

Hello! Thanks for taking the time to do our technical test. We know these are annoying, but we need a way to assess everyone that we employ to avoid disappointment on both sides later on.

Please follow the instructions below and when you are done, put your code into a git repo you own and send us a link to it for us to assess. GitHub or BitBucket are both free for public repos.

Feel free to use as much Google-Fu as you need to (that is what we do on a daily basis) - however, you need to do the test on your own. Any outside help is not in the spirit of the test and will result in seven or eight years of bad luck, depending on which is worst for you.

**Impress us with your skills and show us what you can do! This is your chance to shine and the more we see the better.**


# Pre-requesites

You will need:

- an IDE of your choice
- web browser

#�Getting Started

Look at the API documentation for this API:

https://api.interview.healthforge.io

There are two resources:

* `patient` - no authentication required
* `patient-secure` - requires a bearer token

Use the `Authorize` link at the top of the Swagger UI and log in with the following credentials:

- Username: `interview`
- Password: `Interview01`

You should now be able to access the secure patient resources.

# Question 1

We would like you to create an application that uses the API above and displays a list of patients.

You can use any frameworks and technologies you like.

## Requirements:

### Must Have

- Display a list of patients with header columns
- Clicking on a patient in the list should display that patient's details

### Should Have

- Allow searching
- Implement paging
- Implement sorting
- Implement filtering
- Look half-decent

### Could Have

- Good UX
- Good looking styling


No authentication is required, so use the `patient` resource.

*If you encounter CORS issues, run your app on `http://localhost:4444`.*

This is what we are expecting, but feel free to adjust if you can make it better:

![alt text](patient_list.png "Patient List")

# Question 2

Create a copy of your application in Question 1 and modify it to use the `patient-secure` resource. The user of the application is required to supply login details (username=`interview` and password=`Interview01`).

![alt text](with_auth.png "With authentication")

You will need to use our authentication server (OpenID Connect) with the following details:

* token url: `https://auth.healthforge.io/auth/realms/interview/protocol/openid-connect/token`
* client id: `interview`
* no client secret is required as it is public
* allowed origins: `http://localhost:4444`
* Ideally you should use Hybrid Flow - *if you are having problems with this, see the section below on Direct Grants*

**Your app should be run on `http://localhost:4444` to avoid CORS issues with our authentication server.**

We recommend that you use the Keycloak Javascript Adapter (`npm install keycloak-js`) to interact with the our authentication server:

https://www.keycloak.org/docs/3.0/securing_apps/topics/oidc/javascript-adapter.html

You will need to supply `keycloak.json`:

```
{
  "realm": "interview",
  "auth-server-url": "https://auth.healthforge.io/auth",
  "ssl-required": "external",
  "resource": "interview",
  "public-client": true
}
```

Or you can configure the client adapter in code:

```
var keycloak = Keycloak({
    url: 'https://auth.healthforge.io/auth',
    realm: 'interview',
    clientId: 'interview'
});
```

The OpenID Connect client that you will be using is public, so it does not require a client secret to be set.

And example of how to do this with Angular can be found here:

https://github.com/keycloak/keycloak/tree/master/examples/demo-template/angular-product-app/src/main/webapp

## Direct Grants

If you are really having trouble getting OpenID Connect's hybrid flow to work, you could also try to get this working using Direct Grants.

You will need to **make your own login page** to ask the user for their credentials and give them to our authentication server to get an `access token`.

Here is a shell script that shows how to get the token by doing a `POST` to the token endpoint and then passing that token in the headers of a `GET` request to the secure patient API:

```sh
#!/bin/bash

export TKN=$(curl -X POST 'https://auth.healthforge.io/auth/realms/interview/protocol/openid-connect/token' \
 -H "Content-Type: application/x-www-form-urlencoded" \
 -d "username=interview" \
 -d 'password=Interview01' \
 -d 'grant_type=password' \
 -d 'client_id=interview' | jq -r '.access_token')

curl -k -X GET 'https://api-interview.platform.healthforge.io/api/secure/patient' \
-H "Accept: application/json" \
-H "Authorization: Bearer $TKN" | jq .
```

The script above needs `jq` which you install on MacOS by running `brew install jq` or on Windows `chocolatey install jq`.